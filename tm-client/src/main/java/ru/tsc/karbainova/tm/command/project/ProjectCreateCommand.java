package ru.tsc.karbainova.tm.command.project;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TerminalUtil;
import ru.tsc.karbainova.tm.endpoint.Session;

public class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String name() {
        return "create-project";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create project";
    }

    @Override
    public void execute() {

        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        Session session = serviceLocator.getSession();
        serviceLocator.getProjectEndpoint().createProjectAllParam(session, name, description);
        System.out.println("[OK]");
    }
}
