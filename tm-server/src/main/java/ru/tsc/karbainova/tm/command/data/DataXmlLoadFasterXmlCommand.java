package ru.tsc.karbainova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataXmlLoadFasterXmlCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "xml-faster-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Xml faster load";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NonNull final ObjectMapper objectMapper = new XmlMapper();
        @NonNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[0];
    }
}
