package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.util.Base64;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

import static javax.xml.crypto.dsig.Transform.BASE64;

public class DataBase64SavaCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "data-save-base64";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Save base64 data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final Domain domain = getDomain();
        @NonNull final File file = new File(FILE_BINARY64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NonNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NonNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();

        @NonNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NonNull final String base64 = new String(Base64.getEncoder().encode(bytes));

        @NonNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BINARY64);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
