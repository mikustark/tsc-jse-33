package ru.tsc.karbainova.tm.exception;

public class AccessForbiddenException extends AbstractException {
    public AccessForbiddenException() {
        super("Недостаточно прав");
    }
}
