package ru.tsc.karbainova.tm.component;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.karbainova.tm.api.repository.*;
import ru.tsc.karbainova.tm.api.service.*;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.comparator.ComparatorCommand;
import ru.tsc.karbainova.tm.constant.TerminalConst;
import ru.tsc.karbainova.tm.endpoint.*;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.AbstractException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.system.UnknowCommandException;
import ru.tsc.karbainova.tm.repository.*;
import ru.tsc.karbainova.tm.service.*;

import javax.xml.ws.Endpoint;
import java.util.stream.Collectors;

public class Bootstrap implements ServiceLocator {

    private final IPropertyService propertyService = new PropertyService();

    private final IUserRepository userRepository = new UserRepository();

    private final IAdminUserService adminUserService = new AdminUserService(userRepository, propertyService);

    private final IUserService userService = new UserService(userRepository, propertyService);

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectToTaskService projectToTaskService = new ProjectToTaskService(projectRepository, taskRepository);

    private final ILogService logService = new LogService();

    private final IAuthService authService = new AuthService(userService, propertyService);

    private final Backup backup = new Backup(this);

    private final FileScanner fileScanner = new FileScanner(this);

    private final CalcEndpoint calcEndpoint = new CalcEndpoint();

    private final ISessionRepository sessionRepository = new SessionRepository();
    private final ISessionService sessionService = new SessionService(sessionRepository, this, propertyService);

    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(this, projectService, projectToTaskService);

    private final TaskEndpoint taskEndpoint = new TaskEndpoint(this, taskService);

    private final UserEndpoint userEndpoint = new UserEndpoint(this, userService);

    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this, adminUserService, sessionService);

    private final AuthEndpoint authEndpoint = new AuthEndpoint(this, authService);

    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(this, sessionService);

    private void initEndpoint(final String wsdl, final Object endpoint) {
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initEndpoint() {
        initEndpoint("http://localhost:8080/CalcEndpoint?wsdl", calcEndpoint);
        initEndpoint("http://localhost:8080/ProjectEndpoint?wsdl", projectEndpoint);
        initEndpoint("http://localhost:8080/TaskEndpoint?wsdl", taskEndpoint);
        initEndpoint("http://localhost:8080/UserEndpoint?wsdl", userEndpoint);
        initEndpoint("http://localhost:8080/AuthEndpoint?wsdl", authEndpoint);
        initEndpoint("http://localhost:8080/AdminUserEndpoint?wsdl", adminUserEndpoint);
        initEndpoint("http://localhost:8080/SessionEndpoint?wsdl", sessionEndpoint);
    }

    public void init() throws Exception {
        displayWelcome();
        initDate();
        initEndpoint();
    }

    public void start(@NonNull final String... args) {
        initPID();
        initCommand();
        if (args.length == 0) {
            startInput();
        } else
            try {
                logService.info("Program start with arg.");
                executeCommandByArg(args[0]);
            } catch (@NonNull final AbstractException e) {
                logService.error(e);
            }
    }

    @SneakyThrows
    private void initPID() {
        @NonNull final String filename = "task-manager.pid";
        @NonNull final String pid = Long.toString(getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NonNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private static long getPID() {
        final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (@NonNull Exception e) {
                return 0;
            }
        }
        return 0;
    }

    public void startInput() {
        displayWelcome();
        initDate();
        logService.debug("Test environment");
        initEndpoint();
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        fileScanner.init();
        backup.init();
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            try {
                System.out.println("Enter command:");
                command = scanner.nextLine();
                logService.command(command);
                executeCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
        backup.stop();
    }

    public void executeCommand(@NonNull final String commandName) {
        if (commandName.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(commandName);
        if (abstractCommand == null) throw new UnknowCommandException(commandName);
        final Role[] roles = abstractCommand.roles();
        authService.checkRole(roles);
        abstractCommand.execute();
    }

    public void executeCommandByArg(@NonNull final String argName) {
        if (argName.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByArg(argName);
        if (abstractCommand == null) throw new UnknowCommandException(argName);
        final Role[] roles = abstractCommand.roles();
        authService.checkRole(roles);
        abstractCommand.execute();
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }


    private void initDate() {
        String idTest = userService.create("test", "test", "test").getId();
        String idAdmin = adminUserService.create("admin", "admin", Role.ADMIN).getId();

//        projectService.create(idAdmin, "p1", "d1");
//        projectService.create(idAdmin, "p2", "d2");
//        projectService.create(idAdmin, "p3", "d3");
//
//        taskService.create(idAdmin, "t1", "d1");
//        taskService.create(idAdmin, "t2", "d2");
//        taskService.create(idAdmin, "t3", "d3");
    }

    @SneakyThrows
    public void initCommand() {
        final Reflections reflections = new Reflections("ru.tsc.karbainova.tm.command");
        final List<Class<? extends AbstractCommand>> classes = reflections
                .getSubTypesOf(ru.tsc.karbainova.tm.command.AbstractCommand.class)
                .stream()
                .sorted(ComparatorCommand.getInstance())
                .collect(Collectors.toList());
        for (@NonNull final Class<? extends AbstractCommand> clazz : classes) {
            if (Modifier.isAbstract(clazz.getModifiers())) continue;
            registryCommand(clazz.newInstance());
        }
    }

    public void registryCommand(@NonNull final AbstractCommand command) {
        try {
            @NonNull final String terminalCommand = command.name();
            @NonNull final String terminalDescription = command.description();
            @Nullable final String terminalArg = command.arg();
            if (terminalCommand.isEmpty()) throw new EmptyNameException();
            if (terminalDescription.isEmpty()) throw new EmptyNameException();
            command.setServiceLocator(this);
            commandService.getCommands().put(terminalCommand, command);
            if (terminalArg == null || terminalArg.isEmpty()) return;
            commandService.getArguments().put(terminalArg, command);
        } catch (@NonNull AbstractException e) {
            logService.error(e);
        }
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectToTaskService getProjectToTaskService() {
        return projectToTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @Override
    public IAdminUserService getAdminUserService() {
        return adminUserService;
    }
}
