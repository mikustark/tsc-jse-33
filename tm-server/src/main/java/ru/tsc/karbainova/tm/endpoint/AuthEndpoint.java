package ru.tsc.karbainova.tm.endpoint;

import lombok.NonNull;
import ru.tsc.karbainova.tm.api.service.IAuthService;
import ru.tsc.karbainova.tm.api.service.ServiceLocator;
import ru.tsc.karbainova.tm.model.Session;
import ru.tsc.karbainova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AuthEndpoint {
    private IAuthService authService;
    private ServiceLocator serviceLocator;

    public AuthEndpoint() {
    }

    public AuthEndpoint(
            final ServiceLocator serviceLocator,
            final IAuthService authService
    ) {
        this.authService = authService;
        this.serviceLocator = serviceLocator;
    }

    @WebMethod
    public void logoutAuth(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        authService.logout();
    }

    @WebMethod
    public void loginAuth(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password
    ) {
//        serviceLocator.getSessionService().validate(session);
        authService.login(login, password);
    }

    @WebMethod
    public void registryAuth(
            @WebParam(name = "session") final Session session,
            @WebParam(name = "login") @NonNull String login,
            @WebParam(name = "password") String password,
            @WebParam(name = "email") String email) {
        serviceLocator.getSessionService().validate(session);
        authService.registry(login, password, email);
    }

    @WebMethod
    public User getUser(
            @WebParam(name = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        final String userId = session.getUserId();
        return serviceLocator.getUserService().findById(userId);
    }
}
