package ru.tsc.karbainova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.FileOutputStream;

public class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "yaml-faster-save";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Yaml faster save";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final Domain domain = getDomain();
        @NonNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NonNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NonNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_YAML);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
