package ru.tsc.karbainova.tm.repository;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.service.AuthService;

import java.util.List;

public class ProjectRepositoryTest {

    @Nullable
    private ProjectRepository projectRepository;
    @Nullable
    private Project project;
    private final String userLogin = "test";

    @Before
    public void before() {
        projectRepository = new ProjectRepository();
        projectRepository.add(userLogin, new Project("Project"));
        project = projectRepository.entities.get(0);
    }

    @Test
    public void add() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(project.getId());
        Assert.assertNotNull(project.getName());
        Assert.assertEquals("Project", project.getName());

        @NonNull final Project projectById = projectRepository.findById(project.getUserId(), project.getId());
        Assert.assertNotNull(projectById);
    }

    @Test
    public void findAll() {
        @NonNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByUserId() {
        @NonNull final List<Project> projects = projectRepository.findAll(userLogin);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findAllByErrorUserId() {
        @NonNull final List<Project> projects = projectRepository.findAll("ertyut");
        Assert.assertNotEquals(1, projects.size());
    }

    @Test
    public void findAllByName() {
        @NonNull final Project projects = projectRepository.findByName(userLogin, project.getName());
        Assert.assertNotNull(projects);
    }

    @Test
    public void findAllByErrorName() {
        @NonNull final Project projects = projectRepository.findByName(userLogin, "sdf");
        Assert.assertNull(projects);
    }

    @Test
    public void removeById() {
        projectRepository.remove(userLogin, project);
        Assert.assertNull(projectRepository.findById(userLogin, project.getId()));
    }

    @Test
    public void removeByErrorUserId() {
        @NonNull final Project projects = projectRepository.removeById("sd", project.getId());
        Assert.assertNull(projects);
    }
}
